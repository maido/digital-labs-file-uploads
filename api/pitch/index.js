const axios = require('axios');
const bugsnag = require('@bugsnag/js');
const bugsnagExpress = require('@bugsnag/plugin-express');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const crypto = require('crypto');
const express = require('express');
const FormData = require('form-data');
const fs = require('fs');
const get = require('lodash/get');
const map = require('lodash/map');
const multer = require('multer');
const omit = require('lodash/omit');
const path = require('path');

const app = express();

const dev = process.env.NODE_ENV !== 'production';
const isBugsnagEnabled = !dev && process.env.BUGSNAG_CLIENT_ID;
let bugsnagClient;

if (isBugsnagEnabled) {
    bugsnagClient = bugsnag({
        apiKey: process.env.BUGSNAG_CLIENT_ID,
        beforeSend: report =>
            new Promise((resolve, reject) => {
                if (process.env.NODE_ENV === 'development') {
                    resolve(false);
                } else {
                    resolve();
                }
            }),
        releaseStage: process.env.NODE_ENV
    });
    bugsnagClient.use(bugsnagExpress);

    const middleware = bugsnagClient.getPlugin('express');

    app.use(middleware.requestHandler);
    app.use(middleware.errorHandler);
}

app.use(cookieParser());
app.use(cors());

const UPLOAD_PATH = '/tmp';
const storage = multer.diskStorage({
    dest: `${UPLOAD_PATH}/`,
    filename: (req, file, cb) => {
        crypto.pseudoRandomBytes(16, function(err, raw) {
            cb(null, `${raw.toString('hex')}.${path.extname(file.originalname).replace('.', '')}`);
        });
    },
    fileFilter: (req, file, cb) => {
        if (file.originalname !== 'pdf') {
            return cb(new Error('File type not supported'), false);
        }

        cb(null, true);
    }
});
const MAX_FILE_SIZE = 1024 * 1024 * 15; // 15 MB
const limits = {
    fieldSize: MAX_FILE_SIZE,
    fileSize: MAX_FILE_SIZE,
};
const upload = multer({storage, limits});

app.options('*');
app.post('*', upload.single('file'), (req, res) => {
    try {
        const form = new FormData();
        // $FlowFixMe
        const additionalFields = omit(req.body, ['file', 'isFileRequired', 'apiUrl', 'token']);

        if (req.file) {
            form.append('file', fs.createReadStream(get(req, 'file.path')));
        }

        map(additionalFields, (value, key) => {
            form.append(key, value);
        });

        axios
            .post(`${process.env.API_URL}${req.body.apiUrl}`, form, {
                headers: {
                    Authorization: `Bearer ${req.body.token}`,
                    ...form.getHeaders()
                }
            })
            .then(response => {
                res.status(200).json(response.data.data);
            })
            .catch(err => {
                if (bugsnagClient) {
                    bugsnagClient.notify(err);
                }

                res.status(err.response.status).json(err.response.data);
            });
    } catch (err) {
        if (bugsnagClient) {
            bugsnagClient.notify(err);
        }

        res.sendStatus(400);
    }
});

export default app;
